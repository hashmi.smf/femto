import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core'
import {AuthService} from '../services/auth.service';
import template from './register-form.component.html'

@Component({
  selector: 'register-form',
  providers: [AuthService],
  template: template
})

export class RegisterFormComponent implements OnInit {
  signUpUser = {
    email: '',
    password: '',
    passwordConfirmation: ''
  }
  constructor(public authservice: AuthService){}
  @Output() onFormResult = new EventEmitter<any>();
  ngOnInit(){}
  onRegisterFormSubmit(){
    this.authservice.registerUser(this.signUpUser).subscribe(
      res => {
        if(res.status === 200){
          this.onFormResult.emit({signedUP: true, res})
        }
      },

      err => {
        console.log({signedUp: false, err})
      }
    )
  }
}
