import { Component } from '@angular/core';
import {Angular2TokenService} from "angular2-token";
import template from './app.component.html'
@Component({
  selector: 'femto',
  template: template
})
export class AppComponent {
  
  constructor(private authToken: Angular2TokenService){
    this.authToken.init({
      apiBase: 'http://localhost:5000'
    });

    // this.authToken.signIn({email: "admin@mailinator.com", password: "admin123"}).subscribe(
    //   res => {
    //     debugger
    //     console.log('auth response', res);
    //   },

    //   err => {
    //     console.log('auth error', err)
    //   }

    // )
  }
}
