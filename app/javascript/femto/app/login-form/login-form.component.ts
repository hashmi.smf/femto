import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import template from './login-form.component.html'
import {AuthService} from '../services/auth.service'

@Component({
  selector: 'login-form',
  template: template,
  providers: [AuthService]

})

export class LoginFormComponent implements OnInit{
  signInUser = {
    email: '',
    password: ''
  }
  constructor(private authservice: AuthService){}
  @Output() onFormResult = new EventEmitter<any>()
  ngOnInit(){}
  onSignInSubmit(){
    this.authservice.logInUser(this.signInUser).subscribe(
      res => {
        if (res.status === 200){
          this.onFormResult.emit({signedIn: true, res});
        }
      },
      
      err => {
        console.log({signedIn: false, err})
      }
         
    )
  }
}
