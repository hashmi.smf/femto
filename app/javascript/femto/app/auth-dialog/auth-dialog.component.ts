import {Component, ViewChild,OnInit, EventEmitter, Input} from '@angular/core'
import {Router} from '@angular/router'

import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import template from './auth-dialog.component.html'

@Component({
  selector: 'auth-dialog',
  providers: [NgbActiveModal, NgbModalRef],
  template: template

})

export class AuthDialogComponent {
  @Input('auth-mode') authMode: 'login' | 'register' = 'login'
  @ViewChild('content') public content:NgbModal;

  constructor(private modalService: NgbModal,
              public closeModal: NgbActiveModal,
              public router: Router){}

  authDialog(mode){
    this.authMode = mode
    this.modalService.open(this.content)
  }

  onLoginFormResult(e){
    if(e.signedIn){
      this.closeModal.close()
      this.router.navigate(['/profile'])
    }else{
      console.log(e)
    }
  }
  
  onRegisterFormResult(e){
    if(e.signedUp){
      this.closeModal.dismiss(e)
      this.closeModal.close()
    }else{
      console.log(e)
    }
  }
  isloginmode(){return this.authMode == 'login'}
  isregistermode(){return this.authMode == 'register'}
}