import {Component, ViewChild} from '@angular/core'
import {Router} from '@angular/router'
import {Angular2TokenService} from "angular2-token";
import template from './navbar.component.html'
import {AuthDialogComponent} from '../auth-dialog/auth-dialog.component'
import {AuthService} from '../services/auth.service'

@Component({
  selector: 'navbar',
  providers: [AuthService],
  template: template
})

export class NavbarComponent {
  @ViewChild('authdialog') authDialog: AuthDialogComponent
  constructor(public authservice: Angular2TokenService,
              private modelAuth: AuthService,
              private router: Router){}
  presentAuthDialog(mode: 'login' | 'register' = 'login'){
    this.authDialog.authDialog(mode)
  }
  
  logOut(){
    this.modelAuth.logOutUser().subscribe(
      res => {
        this.router.navigate(['/'])
      }
    )  
  }
  
}